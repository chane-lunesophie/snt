---
author: Sophie CHANE-LUNE
title: PCSHEET !!!
---

`#` introduit un **commentaire** qui se poursuit jusqu'à la fin de la ligne.  

# Les types de base
Une **expression** est une combinaison de constantes, de valeurs des variables, d'opérateurs et des résultats de fonctions qui est évaluée et qui produit une nouvelle valeur.  

??? abstract "Les nombres"

    #### 1. Définition  
    Nombres entiers (`int` pour *"integer"* en anglais) : `5`, `-20`, `987654321`  
    Nombres à virgules flottantes (`float` pour *"floating-point number"* en anglais) : `1.5`, `3.14159`, `-6.5`, `.5`, `-1.`, `5e3`, `-123.4e-5`  

    #### 2. Opérateurs arithmétiques  
    - addition : `+`  
    - soustraction : `-`  
    - multiplication : `*`  
    - division : `/`  
    - puissance : `**`  
    - division entière : `//`  
    - reste de la division entière : `%`  

    Ordre de priorité : `[]`, `()`, `**`, `*`, `/`, `//`, `%`, `+`, `-`
        
??? abstract "Les chaînes de caractères"

    #### 1. Définition  
    Chaînes de caractères (`string` pour *"string of characters* en anglais) : "NSI", 'NSI'  

    #### 2. Opérateurs sur les chaînes de caractères  


??? abstract "Les booléens"

    #### 1. Définition  
    Booléens (`bool` pour *"boolean"* en anglais) : `True`, `False`  

    #### 2. Opérateurs logiques  
    - `or`
    - `and`
    - `not`

    Priorité croissante des opérateurs dans les expressions :  
    `or`, `and`, `not`, `in`, `not in`, `==`, `!=`, `<`, `<=`, `>`, `>=`, `+`, `-`, `*`, `/`, `//`, `%`, `**`, `()`, `[]`  

# Les structures de contrôle
??? abstract "Les conditionnelles"
    !!! note "SI"

        <!-- Conditionnelle -->
        - Si la `condition` est vraie (`True`), le bloc d'instructions indenté après le test est exécuté.  
        - Si la `condition` est fausse (`False`), elle ne sont pas exécutées.

        ```python
        if condition :                  # Ne pas oublier le signe de ponctuation :
            bloc_instructions_1         # Attention à l'indentation
        ``` 

    !!! note "SI ALORS"

        <!-- Alternative -->
        On peut également préciser des instructions à effectuer si la `condition` est fausse, à l’aide de l’instruction `else` :
        ```python
        if condition :
            bloc_instructions_1
        else :
            bloc_instructions_2
        ``` 

    !!! note "SI ALORS SINON SI"
    
        <!-- Alternative -->
        L’instruction `elif` est équivalente à `else if`.
        ```python
        if condition :
            bloc_instructions_1
        elif :
            bloc_instructions_2
        elif :
            bloc_instructions_3
        ...
        else :
            bloc_instructions
        ``` 

??? abstract "Les boucles"
    ??? abstract "Boucle bornée"

        !!! note "POUR ... ALLANT JUSQU'À ..."

            Pour exécuter `n` fois le bloc d'instructions indenté, à l’aide d’une variable entière `variable` allant de `0` à `n-1` :
            ```python
            for variable in range(n) :
                bloc_instructions
            ```

        !!! note "POUR ... ALLANT DE ... JUSQU'À ..."

            Si la variable varie d’un entier `m` à `n-1` (`n>m`), on remplace la première ligne par :
            ```python
            for variable in range(m,n) :
                bloc_instructions
            ```

        !!! note "POUR ... ALLANT DE .. JUSQU'À ... PAR PAS DE ..."

            Si la variable varie d’un entier `m` à `n-1` (`n>m`) par pas de `p`, on remplace la première ligne par :
            ```python
            for variable in range(m,n,p) :
                bloc_instructions
            ```

    ??? abstract "Boucle non bornée"

        !!! note "TANT QUE"
            Pour exécuter en boucle le bloc d'instructions indenté tant que la condition est vraie (`True`) :
            ```python
            while condition :
                bloc_instructions
            ```
# Les fonctions
---
author: Sophie CHANE-LUNE
title: Programmation en Python
---

# Notions de programmation avec Python

## Affectations, variables  

## Séquences  

## Instructions conditionnelles  

## Boucles bornées et non bornées  

## Définitions et appels de fonctions  

!!! tip "Ce qu'il faut savoir et savoir faire"

    Écrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux.
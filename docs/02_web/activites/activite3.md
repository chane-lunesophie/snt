---
author: Sophie CHANE-LUNE
title: Activité 3 Créer un site web !
---

## Structure d'une page Web
```html
<!DOCTYPE html>
<html>
    <head>
        Eléments HTML contenant des informations sur la page web qui ne seront pas affichés par le navigateur
    </head>

    <body>
        Elements HTML contenant du contenu à afficher
    </body>
</html>
```

??? note pliée "Corrigé"

    Dates importantes :  
    - 1965 : invention et programmation du concept d’hypertexte par [Ted Nelson](https://fr.wikipedia.org/wiki/Ted_Nelson){target="_blank" rel="noopener"}  
    - 1989 : naissance au CERN par [Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee){target="_blank" rel="noopener"}  
    - 1991 : naissance du langage `HTML`  
    - 1993 : mise dans le domaine public, disponibilité du premier navigateur Mosaic   
    - 1995 : mise à disposition de technologies pour le développement de site Web interactif (langage `JavaScript`) et dynamique (langage `PHP`)   
    - 2001 : standardisation des pages grâce au DOM (Document Object Model)   
    - 2010 : mise à disposition de technologies pour le développement d’applications sur mobiles  

---
author: Sophie CHANE-LUNE
title: Activité 1 Repères historiques
---

Regardez cette vidéo, puis répondez à la question suivante :
<iframe width="560" height="315" src="https://www.youtube.com/embed/YVn7jrDYjUQ" title="Découvrons l&#39;histoire du Web" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Réaliser, à l’aide de l’outil [frisechronos](http://www.frisechronos.fr/dojomain.htm){:target="_blank" }, une frise chronologique comportant les dates des événements de l'histoire du Web cités dans cette vidéo. Chacun de ces événements sera illustré par une image et un commentaire.

??? note pliée "Corrigé"

    Dates importantes :  
    - 1965 : invention et programmation du concept d’hypertexte par [Ted Nelson](https://fr.wikipedia.org/wiki/Ted_Nelson){target="_blank" rel="noopener"}  
    - 1989 : naissance au CERN par [Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee){target="_blank" rel="noopener"}  
    - 1991 : naissance du langage `HTML`  
    - 1993 : mise dans le domaine public, disponibilité du premier navigateur Mosaic   
    - 1995 : mise à disposition de technologies pour le développement de site Web interactif (langage `JavaScript`) et dynamique (langage `PHP`)   
    - 2001 : standardisation des pages grâce au DOM (Document Object Model)   
    - 2010 : mise à disposition de technologies pour le développement d’applications sur mobiles  

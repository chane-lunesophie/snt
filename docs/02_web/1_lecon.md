---
author: Sophie CHANE-LUNE
title: LE WEB
---
### Introduction
Le **Web** (encore appelé *toile*), abréviation de "**World Wide Web**" en anglais, désigne un système mondial d'information en ligne qui permet aux utilisateurs d'accéder à des données (documents, médias, applications et diverses ressources électroniques) reliées par des liens hypertextes via le réseau Internet. Le Web permet ainsi aux individus et aux organisations de partager, d'accéder et d'interagir avec une vaste gamme de ressources en ligne.  

*REPÈRES HISTORIQUES*
<iframe width="560" height="315" src="https://www.youtube.com/embed/YVn7jrDYjUQ" title="Découvrons l&#39;histoire du Web" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## I. La navigation sur le Web
L'architecture du Web est basée sur le modèle **client-serveur**, qui permet aux clients (via les navigateurs web par exemple) de demander des ressources ou des fonctionnalités aux serveurs web. Les serveurs web reçoivent ces demandes, les traitent, récupèrent les données nécessaires et renvoient les réponses aux clients. Voici une description de l'architecture du Web :  
 ![Modèle Client-Serveur](./images/client-serveur.png "Modèle Client-Serveur"){ width=50%; : .center }  
- Le **client** est généralement l'utilisateur final ou le logiciel qu'il utilise, tel qu'un navigateur web (comme Chrome, Firefox, Edge, etc.), une application mobile ou un autre dispositif connecté à Internet. Le client envoie une demande HTTP au serveur pour spécifier ce qu'il souhaite faire. Cette demande est généralement initiée par l'utilisateur, par exemple, en cliquant sur un lien, en soumettant un formulaire ou en saisissant une URL dans la barre d'adresse du navigateur.  
- Le **serveur** est un ordinateur ou un système informatique qui héberge des ressources web, telles que des pages web, des images, des vidéos, des fichiers, des applications, des services, etc. Le serveur est configuré pour recevoir les demandes HTTP des clients, traiter ces demandes et renvoyer les réponses appropriées.  

!!! info "Définition : HTTP (*HyperText Transfer Protocol*)"

    Le **HTTP** (**protocole de transfert hypertexte** en français), est un protocole de communication utilisé pour le transfert de données sur le Web, entre le client et le serveur. Les demandes HTTP sont envoyées par le client, et les réponses HTTP sont renvoyées par le serveur.  
    !!! warning "Remarque"
    
        **HTTPS** (*HTTP Secure*) est la version sécurisée du protocole HTTP. En utilisant ce protocole, les communications sont chiffrées et ne peuvent pas être comprises par un intermédiaire qui intercepterait les messages.

Les ressources présentes sur le Web sont localisées par une **URL**.

!!! info "Définition : URL (*Uniform Resource Locator*)"
    L'**URL** (**localisateur uniforme de ressource** en français) est l'adresse qui spécifie la localisation des ressources sur le web. Cette adresse unique comprend le nom du protocole, le nom de la machine qui héberge la ressource, le nom de la ressource et éventuellement des paramètres optionnels. 
 
    Par exemple : *https://www.example.com/page/index.html*  
    Cette URL désigne la page nommée *index.html* du dossier *page* présente sur le serveur *example.com* avec le protocole *https*.

!!! info "Définition : Liens hypertextes"
    Les pages web contiennent du texte possédant souvent des **liens hypertextes**, qui permettent aux utilisateurs de naviguer d'une page à une autre en cliquant sur des liens. Ces liens hypertextes sont spécifiés dans le code HTML des pages web.

!!! warning "Remarque"
    Quelle est la différence entre internet et Web ?  
    <iframe width="560" height="315" src="https://www.youtube.com/embed/GqD6AiaRo3U" title="MOOC SNT / Le web, &quot;site internet&quot; ou &quot;site web&quot; ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
## II. Les moteurs de recherche
Les **moteurs de recherche** sont des outils conçus pour aider les utilisateurs à trouver des informations en ligne dont on ne connaît pas l’adresse, voire dont on ignore l’existence. Pour cela, les moteurs de recherche parcourent en permanence le web, répertorient et indexent les pages et fournissent des résultats pertinents aux utilisateurs en fonction de leurs requêtes.  

### Indexation
Les moteurs de recherche utilisent des programmes automatisés appelés "robots d'indexation" ou "spiders" pour explorer le web en suivant des liens hypertextes. Ces robots visitent les sites web, analysent leur contenu et indexent les informations pertinentes. Lors de l'**indexation**, les moteurs de recherche enregistrent des données sur les pages web, telles que les mots-clés, les titres, les balises méta, les liens, etc.
Ces données indexées sont ensuite stockées dans une vaste base de données appelée "*index*" pour faciliter la recherche ultérieure.  

Lorsqu'un utilisateur saisit une requête dans la barre de recherche d'un moteur de recherche, le moteur compare cette requête aux données stockées dans son index et utilise des algorithmes complexes pour déterminer la pertinence des pages web indexées par rapport à la requête de l'utilisateur.
Les résultats de la recherche sont générés en fonction de cette pertinence, avec les pages les plus pertinentes apparaissant en haut de la liste.

!!! warning "Remarques"
    - Les moteurs de recherche peuvent également proposer d'autres types de résultats, tels que des images, des vidéos, des actualités, des cartes, etc.  
    - Les utilisateurs peuvent souvent trier et filtrer les résultats en fonction de différents critères tels que la date, la pertinence, la localisation, etc. Certains moteurs de recherche proposent également des outils de recherche avancés pour affiner les résultats.

### Le référencement naturel
Le **référencement naturel**, souvent abrégé en SEO (*Search Engine Optimization* en anglais) désigne l'ensemble des techniques et des pratiques visant à améliorer la visibilité et le classement d'un site web dans les résultats de recherche des moteurs de recherche tels que Google, Bing, Yahoo, et d'autres, grâce notamment à des mots-clés placés dans le contenu rédactionnel. Les résultats de recherche sont les résultats non payants, c'est-à-dire ceux qui apparaissent naturellement en fonction de l'algorithme de recherche du moteur, par opposition aux résultats payants ou publicitaires. Le référencement naturel est à distinguer du référencement payant des annonces et les liens sponsorisés.

## III. Les langages du Web
L'affichage d'une page web est réalisé grâce au navigateur en combinant plusieurs technologies, dont le **HTML** (*HyperText Markup Language*), les feuilles de style **CSS** (*Cascading Style Sheets*), et les langages de programmation côté client comme le **JS** (*JavaScript*).  

### HTML : le langage du contenu
Le **HTML** (*HyperText Markup Language*) ou en français "langage de balisage hypertexte", est le langage de balisage utilisé pour créer et structurer le contenu des pages web. Il définit la manière dont le contenu est organisé et affiché dans un navigateur web en ajoutant des **balises**.  

Voici la structure minimale d'une page Web :
```html
<!DOCTYPE html>
<html>
    <head>
        Eléments HTML contenant des informations sur la page web qui ne seront pas affichés par le navigateur
    </head>

    <body>
        Elements HTML contenant du contenu à afficher
    </body>
</html>

```
!!! warning "Remarque"
    Une page HTML peut être interprétée différemment par deux navigateurs différents et peut s'adapter à différents types d'apparails : smartphone, tablette ou ordinateur.

Voici un exemple de contenu d'un fichier HTML et son rendu dans un navigateur Web :  

!!! example "Premier exemple"

    === "HTML"
        ```html
            <!DOCTYPE html>
            <html>
                <head>
                    <title> Ma première page Web </title>
                </head>

                <body>
                    <h1>Le titre de le page</h1>
                    <p> 
                        Voici un paragraphe contenant un <a href="page2.html">hyperlien vers une autre page</a>
                    </p>
                    <p>Voici un second paragraphe</p>
                </body>
            </html>
        ```

    === "Rendu navigateur"
        ![Ma première page HTML](./images/page%20web.png "Ma première page HTML")


!!! warning "Remarque"
    Une page HTML contient le texte à afficher encapsulé par des balises ouvrantes `< >` et des balises fermantes `</ >`.  
    
Récapitulatif des balises utilisés  


| Balise                 | Description                                          |
| :--------------------: | :----------------------------------------------------|
| `<html>...</html>`     | Balise principale de la page Web |
| `<head>...</head>`     | En-tête de la page Web            |
| `<body>...</body>`     | Corps de la page Web|
|  | |
| `<title>...</title>`   | Titre de la page Web (se trouve dans la balide `head`)|
|  | |
| `<br/>`                | Permet un retour à la ligne |
| `<h1>...</h1>`         | Titre de sections de niveau 1. Il existe `h2`,`h3`, ... pour des titre de sous-sections, sous-sous-sections etc. | 
| `<p>...</p> `          | Permet d'ajouter un nouveau paragraphe    |
| `<a href="s">...</a> ` | Permet de créer un lien hypertexte ayant pour cible l’url `s`    |
| `<img src="s"> `       | Permet d'insérer une image se trouvant à l'url `s`  |
|  | |
| `<b>...</b>`           | Mise en gras du texte |
| `<i>...</i>`           | Mise en italique du texte |
| `<u>...</u>`           | Mise en souligné du texte |
| `<center>...</center>` | Permet de centrer du texte ou une image |
|  | |
| `<ol>...</ol>`         | Liste énumérée |
| `<ul>...</ul>`         | Liste non ordonnée |
| `<li>...</li> `        | Élément d’une liste. Doit apparaître sous un `ul` ou `ol` |
|  | |
| `<table>...</table>`   | Définit une table HTML |
| `<tr>...</tr>`         | Définit une ligne d’une table HTML. Doit apparaître sous un `table` |
| `<th>...</th>`         | éfinit une ligne d’en-tête de colonnes d’une table HTML. Doit apparaître sous un `table` |
| `<td>...</td>`         | Définit une cellule d’une table HTML. Doit apparaître sous un `tr` ou `th` |


### CSS : le langage du style
Le **CSS**, (*Cascading Style Sheets*) ou Feuilles de style en cascade en français, est un langage utilisé pour décrire la présentation et la mise en forme des documents **HTML** (*HyperText Markup Language*) et **XML** (*eXtensible Markup Language*). En d'autres termes, le **CSS** permet de contrôler l'apparence visuelle des éléments d'une page web, tels que les couleurs, les polices de caractères, la mise en page, les marges, les bordures, etc. Le CSS fonctionne en séparant le contenu (HTML) de la présentation (CSS), ce qui permet une gestion plus efficace et cohérente du style d'une page web.  

!!! example "Premier exemple"

    === "HTML"
        ```html
            <!DOCTYPE html>
            <html>
                <head>
                    <link rel="stylesheet" href="style.css">
                    <title> Ma première page Web </title>
                </head>

                <body>
                    <h1>Le titre de le page</h1>
                    <p> 
                        Voici un paragraphe contenant un <a href="page2.html">hyperlien vers une autre page</a>
                    </p>
                    <p>Voici un second paragraphe</p>
                </body>
            </html>
        ```

    === "CSS"
        ```css
                h1 {
                    color: red;
                    font-size: 24px;
                }
        ```

    === "Rendu navigateur"
        ![Ma première page HTML](./images/page%20web%20css.png "Ma première page HTML")


### JS : le langage des interactions
Le langage **JavaScript** est utilisé pour programmer les interactions avec l'utilisateur.

## IV. Les droits sur le Web
- Sécurité et confidentialité  
En formulant des requêtes sur des sites Web dynamiques et en laissant des programmes s’exécuter sur sa machine, l’utilisateur prend des risques : il peut communiquer des informations personnelles à son insu à des serveurs qui en gardent une trace, à distance ou localement par des cookies, ou encore charger des pages contenant des programmes malveillants, par exemple permettant d’espionner en continu les actions de l’utilisateur. Par ailleurs, un navigateur peut garder un historique de toutes les interactions, et le laisser accessible aux sites connectés. L’utilisateur peut utiliser des services qui s’engagent à ne pas garder de traces de ses interactions, par exemple certains moteurs de recherche. Il peut aussi paramétrer son navigateur de façon à ce que celui-ci n’enregistre pas d’historique des interactions. De fausses pages peuvent encore être utilisées pour l’hameçonnage des utilisateurs. Un nom de lien pouvant cacher une adresse Web malveillante, il faut examiner cette adresse avant de l’activer par un clic.

- Impacts sur les pratiques humaines  
Dans l’histoire de la communication, le Web est une révolution : il a ouvert à tous la possibilité et le droit de publier ; il permet une coopération d’une nature nouvelle entre individus et entre organisations : commerce en ligne, création et distribution de logiciels libres multi-auteurs, création d’encyclopédies mises à jour en permanence, etc. ; il devient universel pour communiquer avec les objets connectés.
Le Web permet aussi de diffuser toutes sortes d’informations dont ni la qualité, ni la pertinence, ni la véracité ne sont garanties et dont la vérification des sources n’est pas toujours facile. Il conserve des informations, parfois personnelles, accessibles partout sur de longues durées sans qu’il soit facile de les effacer, ce qui pose la question du droit à l’oubli. Il permet une exploitation de ses données, dont les conséquences sociétales sont encore difficiles à estimer : recommandation à des fins commerciales, bulles informationnelles, etc. En particulier, des moteurs de recherche permettent à certains sites d’acquérir de la visibilité sur la première page des résultats de recherche en achetant de la publicité qui apparaîtra parmi les liens promotionnels.

### Conclusion
<iframe width="560" height="315" src="https://www.youtube.com/embed/g1rvdtOUWA4" title="Le web (SNT 2de)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


!!! tip "Ce qu'il faut savoir et savoir faire"

    - Connaître les étapes du développement du Web  
    - Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien)  
    - Maîtriser les renvois d’un texte à différents contenus  
    - Distinguer ce qui relève du contenu d’une page et de son style de présentation  
    - Etudier et modifier une page HTML simple  
    - Décomposer l’URL d’une page et reconnaître les protocoles
    - Reconnaître les pages sécurisées  
    - Décomposer le contenu d’une requête HTTP et identifier les paramètres passés  
    - Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur  
    - Mener une analyse critique des résultats fournis par un moteur de recherche  
    - Comprendre les enjeux de la publication d’informations  
    - Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d’un navigateur  
    - Sécuriser sa navigation en ligne et analyser les pages et fichiers  
      